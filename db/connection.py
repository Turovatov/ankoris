from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from db.base import Base
from tool.config import config_mgr


class DatabaseConnectionManager(object):
    """
    Менеджер для работы с БД
    """

    def __init__(self):
        self.engine = None
        self.session = None

    @contextmanager
    def session_scope(self):
        """
        Контекст для работы с бд в транзакции
        :return:
        """
        self.session = Session(self.engine)
        try:
            yield self.session
            self.session.commit()
        except Exception as exc:
            self.session.rollback()
            raise exc
        finally:
            self.session.close()
            self.session = None

    def init_engine(self) -> None:
        """
        Подключаемся к Датабазе
        :return: Клиент для работы с БД
        """
        url = self.get_connection_url()
        self.engine = create_engine(url)

        with self.session_scope():
            Base.metadata.create_all(self.engine)

    @staticmethod
    def get_connection_url() -> str:
        """
        Получаем из настроек строку для подключения к БД
        :return:
        """
        config = config_mgr.get_db_values()
        return config.get('uri')


db_mgr = DatabaseConnectionManager()
