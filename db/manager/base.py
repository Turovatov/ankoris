from sqlalchemy import desc

from db.connection import db_mgr


class BaseDataManager(object):
    """
    Базовый класс для менеджеров, работающих с таблицами БД
    """

    table = None

    @staticmethod
    def as_dict(row) -> dict or None:
        """
        Представляем ORM в виде словаря
        :param row: ORM объект
        :return:
        """
        if row:
            columns = row.__table__.columns.keys()
            return dict((col, getattr(row, col)) for col in columns)
        return None

    def get_all(self) -> list:
        """
        Выбрать все записи из таблицы
        :return:
        """
        with db_mgr.session_scope() as session:
            contact_rows = session.query(self.table).order_by(desc(self.table.created_on)).all()
            return [self.as_dict(row) for row in contact_rows]

    def get_by(self, **kwargs) -> dict:
        """
        Выбрать запись из таблицы по параметрам
        :param kwargs: ИД пользователя
        :return:
        """
        with db_mgr.session_scope() as session:
            iter_ = session.query(self.table)
            for col, value in kwargs.items():
                col = 'id' if col == '_id' else col
                iter_ = iter_.filter(getattr(self.table, col) == value)
            iter_ = iter_.first()
            return self.as_dict(iter_)

    def add(self, **kwargs) -> dict:
        """
        Добавить запись в таблицу
        :param kwargs: Данные для создания
        :return:
        """
        with db_mgr.session_scope() as session:
            orm_obj = self.table(**kwargs)
            session.add(orm_obj)
            session.flush()
            return self.as_dict(orm_obj)

    def update(self, _id: int, **kwargs) -> dict:
        """
        Обновить запись в таблице
        :param _id: ID записи
        :param kwargs: Данные для создания
        :return:
        """
        with db_mgr.session_scope() as session:
            orm_obj = session.query(self.table) \
                .filter(self.table.id == _id) \
                .first()

            for col, value in kwargs.items():
                setattr(orm_obj, col, value)

            session.flush()
            return self.as_dict(orm_obj)
