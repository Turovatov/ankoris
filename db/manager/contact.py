from datetime import datetime

from db.manager.base import BaseDataManager
from db.table.contact import ContactTable
from mail.sender import Sender


class ContactManager(BaseDataManager):
    """
    Менеджер для работы с таблицей Contact
    """

    table = ContactTable

    statuses = {
        1: 'Новое обращение',
        2: 'В работе',
        3: 'Выполнено',
        4: 'Отказано'
    }

    def read(self, contact_id: int = None) -> list or dict:
        """
        Получаем список обращений
        contact_id: Идентификатор обращения
        :return:
        """
        if contact_id:
            filter_params = {'id': contact_id}
            return self.get_by(**filter_params)

        return self.get_all()

    def create(self, values: dict) -> dict:
        """
        Создание нового обращения
        :param values: Словарь с данными
        :return: Результат. Словарь вида...
        """

        if values.get('created_on'):
            values['created_on'] = datetime.strptime(values['created_on'], '%d.%m.%Y %H:%M:%S')
        else:
            values['created_on'] = datetime.now()

        db_values = self.add(**values)

        Sender.send_email(
            email=db_values['email'],
            message='Добавлено новое обращение #{0}'.format(db_values['id'])
        )

        return {
            'success': True,
            'msg': 'Обращение добавлено',
            'data': {
                'id': db_values['id']
            }
        }

    def set_status(self, values: dict) -> dict:
        """
        Устанавливаем новый статус
        :param values: Словарик со значениями для установки
        :return:
        """
        _id = values.get('id', 0)
        del values['id']

        old_values = self.read(contact_id=_id)
        db_values = self.update(_id=_id, **values)

        msg = 'В обращении ID: {0} изменен статус с "{1}" на "{2}"'
        msg = msg.format(
            old_values['id'],
            self.statuses[old_values['status']],
            self.statuses[db_values['status']]
        )

        Sender.send_email(
            email=db_values['email'],
            message=msg
        )

        return {
            'success': True,
            'msg': 'Статус обновлен',
            'data': {
                'id': db_values['id']
            }
        }
