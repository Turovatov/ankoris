from datetime import datetime

from sqlalchemy import Column, String, Integer, DateTime

from db.base import Base


class ContactTable(Base):
    """
    Таблица обращений
    """
    __tablename__ = 'contacts'

    id = Column(Integer, primary_key=True)
    created_on = Column(DateTime, nullable=False, default=datetime.now)
    employer = Column(Integer, nullable=False)
    email = Column(String, nullable=False)
    phone = Column(String, nullable=False)
    status = Column(Integer, nullable=False, default=1)
    text = Column(String, nullable=False)
    comment = Column(String)
