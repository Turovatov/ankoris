Ext.application({
    extend: 'QQ.Application',

    name: 'QQ',

    requires: [
        'QQ.*'
    ],

    mainView: 'QQ.view.list.List'
});
