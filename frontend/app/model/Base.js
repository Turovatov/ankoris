Ext.define('QQ.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'QQ.model'
    }
});
