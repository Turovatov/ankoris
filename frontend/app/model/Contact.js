Ext.define('QQ.model.Contact', {
    extend: 'QQ.model.Base',

    requires: [
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.validator.Email',
        'Ext.data.validator.Format',
        'Ext.data.validator.Inclusion',
        'Ext.data.validator.Presence'
    ],

    fields: [
        {
            name: 'created_on',
            type: 'string'
        },
        {
            name: 'employer',
            type: 'int'
        },
        {
            name: 'email',
            type: 'string'
        },
        {
            name: 'phone',
            type: 'string'
        },
        {
            name: 'status',
            type: 'int'
        },
        {
            name: 'text',
            type: 'string'
        },
        {
            name: 'comment',
            type: 'string'
        }
    ],

    validators: {
        created_on: 'presence',
        employer: 'presence',
        email: ['presence', 'email'],
        phone: ['presence', { type: 'format', matcher: /[0-9]+/i }],
        status: ['presence', { type: 'inclusion', list: [1, 2, 3, 4]}],
        text: 'presence'
    },

    proxy: {
        type: 'rest',
        url: 'api/contact',

        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success',
            messageProperty: 'msg'
        }
    }
});
