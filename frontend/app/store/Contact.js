Ext.define('QQ.store.Contact', {
    extend: 'Ext.data.Store',

    alias: 'store.contact',
    storeId: 'contact',

    requires: [
        'Ext.data.proxy.Rest',
        'QQ.model.Contact'
    ],

    model: 'QQ.model.Contact',

    autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'rest',
        url: 'api/contact',

        appendId: false,
        noCache: false,

        pageParam: '',
        sortParam: '',
        limitParam: '',
        startParam: '',

        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success',
            messageProperty: 'msg'
        }
    }
});
