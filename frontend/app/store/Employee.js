Ext.define('QQ.store.Employee', {
    extend: 'Ext.data.Store',

    alias: 'store.employee',
    storeId: 'employee',

    fields: [
        {name: 'id', type: 'int'},
        {name: 'name',  type: 'string'}
    ],

    autoLoad: true,

    data : [
        {id: 1, name: 'Серова А. Е.'},
        {id: 2, name: 'Павлюченко Е. А.'}
    ]
});