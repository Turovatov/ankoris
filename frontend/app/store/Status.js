Ext.define('QQ.store.Status', {
    extend: 'Ext.data.Store',

    alias: 'store.status',
    storeId: 'status',

    fields: [
        {name: 'id', type: 'int'},
        {name: 'name',  type: 'string'}
    ],

    autoLoad: true,

    data : [
        { id: 1, name: 'Новое обращение' },
        { id: 2, name: 'В работе' },
        { id: 3, name: 'Выполнено' },
        { id: 4, name: 'Отказано' },
    ]
});