Ext.define('QQ.view.form.Form', {
    extend: 'Ext.window.Window',

    xtype: 'contactform',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.TextArea',
        'QQ.store.Employee',
        'QQ.store.Status',
        'QQ.view.form.FormController'
    ],

    controller: 'form',

    title: 'Новое обращение',

    frame: true,
    modal: true,
    width: 500,

    items: [
        {
            xtype: 'form',

            defaults: {
                anchor: '100%'
            },

            fieldDefaults: {
                labelAlign: 'right',
                labelWidth: 150,
                msgTarget: 'side'
            },

            defaultType: 'textfield',

            bodyPadding: 20,

            items: [
                {
                    xtype: 'datefield',
                    fieldLabel: 'Дата/время создания',
                    name: 'created_on',
                    itemId: 'created_on',
                    allowBlank:false,
                    format: 'd.m.Y H:i:s',
                    readOnly: true
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Оператор',
                    name: 'employer',
                    allowBlank:false,
                    store: {
                        type: 'employee'
                    },
                    valueField: 'id',
                    displayField: 'name',
                    queryMode: 'local',
                    value: 1,
                    readOnly: true,
                    editable: false
                },
                {
                    fieldLabel: 'Email заявителя',
                    name: 'email',
                    itemId: 'email',
                    vtype: 'email',
                    allowBlank:false,
                    value: 'test@test.com'
                },
                {
                    fieldLabel: 'Телефон заявителя',
                    name: 'phone',
                    allowBlank:false,
                    regex: RegExp('^\\d+$'),
                    regexText: 'Номер телефона должен содержать только цифры'
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Статус',
                    name: 'status',
                    allowBlank:false,
                    store: {
                        type: 'status'
                    },
                    valueField: 'id',
                    displayField: 'name',
                    queryMode: 'local',
                    value: 1,
                    readOnly: true,
                    editable: false
                },
                {
                    xtype: 'textarea',
                    fieldLabel: 'Текст обращения',
                    name: 'text',
                    allowBlank:false
                },
                {
                    xtype: 'textarea',
                    fieldLabel: 'Комментарий',
                    name: 'comment'
                }
            ],

            buttons: [
                {
                    text: 'Зарегистрировать обращение',
                    scale: 'medium',
                    itemId: 'save',
                    disabled: true,
                    formBind: true
                }
            ]
        }
    ]
});