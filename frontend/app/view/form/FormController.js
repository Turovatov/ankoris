Ext.define('QQ.view.form.FormController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.form',

    uses: [
        'Ext.data.StoreManager',
        'QQ.model.Contact'
    ],

    control: {
        '#': {
            afterrender: 'onAfterRender'
        },
        'toolbar > #save': {
            click: 'onSaveClick'
        }
    },

    /**
     * Отправляем новое обращение на сервер для сохранения
     */
    onSaveClick: function () {
        let panel = this.getView().down('form'),
            form = panel.getForm();

        let model = Ext.create('QQ.model.Contact', form.getValues(false));

        model.save({
            success: Ext.Function.bind(this.onSaveSuccess, this),
            failure: Ext.Function.bind(this.onSaveFailure, this)
        })
    },

    /**
     * Если обращение успешно сохранено, мы добавляем модель в список
     * @param record Сохраненная модель
     */
    onSaveSuccess: function (record) {
        let store = Ext.data.StoreManager.lookup('contact');
        store.insert(0, record);
        this.getView().close();
    },

    /**
     * Если при сохранении произошла ошибка, то ... печатаем в консоль
     * @param record Сохраненная модель
     */
    onSaveFailure: function (record) {
        console.log('failure');
        console.log(record);
    },

    /**
     * Устанавливаем фокус на полу ввода Email. Первое поле ввода которое нужно менять
     */
    onAfterRender: function () {
        this.getView().down('#email').focus();
        this.getView().down('#created_on').setValue(new Date());
    }
});