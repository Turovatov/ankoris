Ext.define('QQ.view.list.List', {
    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.button.Button',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.grid.plugin.CellEditing',
        'Ext.plugin.Viewport',
        'Ext.toolbar.Fill',
        'Ext.toolbar.Separator',
        'QQ.store.Employee',
        'QQ.store.Status',
        'QQ.view.list.ListController',
        'QQ.view.list.ListModel'
    ],

    plugins: ['viewport', 'cellediting'],

    controller: 'list',
    viewModel: 'list',

    bind: '{contacts}',

    title: 'Система регистрации обращений',

    tbar: [
        {
            xtype: 'button',
            text: 'Добавить',
            iconCls: 'fa fa-plus',
            itemId: 'new'
        },
        '->',
        {
            xtype: 'label',
            bind: {
                text: 'Оператор: {operator.name}'
            }
        },
        '-',
        {
            xtype: 'label',
            bind: {
                text: 'Обращений: {contacts.count}'
            }
        }
    ],

    columns: [
        {
            text: 'ID',
            dataIndex: 'id'
        },
        {
            text: 'Дата/время создания',
            dataIndex: 'created_on',
            flex: 1
        },
        {
            text: 'Оператор',
            dataIndex: 'employer',
            renderer: function(value) {
                let store = Ext.create('QQ.store.Employee'),
                    model = store.findRecord('id', value);

                return model.get('name');
            },
            flex: 1
        },
        {
            text: 'Email заявителя',
            dataIndex: 'email',
            flex: 1
        },
        {
            text: 'Телефон заявителя',
            dataIndex: 'phone',
            flex: 1
        },
        {
            text: 'Статус',
            dataIndex: 'status',
            flex: 1,
            renderer: function(value) {
                let store = Ext.create('QQ.store.Status'),
                    model = store.findRecord('id', value);

                return model.get('name');
            },
            editor: {
                xtype: 'combobox',
                allowBlank:false,
                store: {
                    type: 'status'
                },
                valueField: 'id',
                displayField: 'name',
                queryMode: 'local',
                editable: false,
                listeners:{
                    expand: function(combo) {
                        combo.getStore().clearFilter();
                        combo.getStore().filterBy(function(record) {
                            if (combo.getValue() === 1) {
                                return Ext.Array.contains([2, 3, 4], record.get('id'));
                            }
                            if (combo.getValue() === 2) {
                                return Ext.Array.contains([3, 4], record.get('id'));
                            }
                            return false;
                        });
                    }
                }
            }
        },
        {
            text: 'Текст обращения',
            dataIndex: 'text',
            flex: 1
        },
        {
            text: 'Комментарий',
            dataIndex: 'comment',
            flex: 1
        }
    ]
});
