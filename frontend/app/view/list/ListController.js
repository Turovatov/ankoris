Ext.define('QQ.view.list.ListController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.list',

    requires: [
        'QQ.view.form.Form'
    ],

    control: {
        'toolbar > #new': {
            click: 'onNewContact'
        }
    },

    /**
     * Показываем оно для ввода обращения
     */
    onNewContact: function () {
        Ext.create({ xtype: 'contactform' }).show();
    }
});
