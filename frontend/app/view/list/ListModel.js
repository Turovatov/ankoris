Ext.define('QQ.view.list.ListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.list',

    requires: [
        'QQ.store.Contact',
        'QQ.store.Employee'
    ],

    data: {
        employeeId: 1
    },

    stores: {
        operators: {
            type: 'employee',
            filters: [{
                property: 'id',
                value: '{employeeId}'
            }]
        },
        contacts: {
            type: 'contact'
        }
    },

    formulas: {
        operator: {
            bind: '{operators}',
            get: function(operators) {
                return operators.getAt(0);
            }
        }
    }
});