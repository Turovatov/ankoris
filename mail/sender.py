import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from tool.config import config_mgr


class Sender(object):
    """
    Менеджер отправки писем
    """

    @staticmethod
    def send_email(email: str, message: str):
        smtp_config = config_mgr.get_smtp_values()
        admin_email = config_mgr.get_admin_values().get('email')

        msg = MIMEMultipart()

        password = smtp_config.get('password')
        msg['From'] = smtp_config.get('email')
        msg['To'] = email
        msg['Subject'] = "Новое обращение"

        msg.attach(MIMEText(message, 'plain'))

        server = smtplib.SMTP('smtp.gmail.com: 587')
        server.starttls()

        server.login(msg['From'], password)

        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.sendmail(msg['From'], admin_email, msg.as_string())

        server.quit()
