import json
import os
from optparse import OptionParser

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi
import tornado.escape

from db.connection import db_mgr
from db.manager.contact import ContactManager
from tool.config import config_mgr

FRONTEND_PATH = 'frontend/build/production/QQ/'


class MainHandler(tornado.web.RequestHandler):
    """
    Класс, управляющий отдачей контента основной страницы
    """

    def data_received(self, chunk):
        pass

    def get(self):
        self.render(FRONTEND_PATH + 'index.html')


class ContactHandler(tornado.web.RequestHandler):
    """
    Класс, обрабатывающий все запросы
    """

    def data_received(self, chunk):
        pass

    def get(self):
        """
        Запрос на чтение списка обращений
        :return:
        """
        data = ContactManager().read()
        return self.write(json.dumps(data, default=str))

    def post(self):
        """
        Запрос на создние нового обращения
        :return:
        """
        body_data = tornado.escape.json_decode(self.request.body)
        del body_data['id']
        result = ContactManager().create(values=body_data)
        return self.write(json.dumps(result, default=str))

    def put(self):
        """
        Запрос на обновление существующего обращения
        :return:
        """
        body_data = tornado.escape.json_decode(self.request.body)
        result = ContactManager().set_status(values=body_data)
        return self.write(json.dumps(result, default=str))


class Application(tornado.web.Application):
    """
    Приложение на Tornado
    """

    def __init__(self):
        settings = {
            'compress_response': True,
            'debug': True
        }
        handlers = (
            (r'/', MainHandler),
            (r'/api/contact', ContactHandler),
            (r'/(.*)', tornado.web.StaticFileHandler, {'path': os.getcwd() + '/' + FRONTEND_PATH}),
        )
        tornado.web.Application.__init__(self, handlers, **settings)


application = Application()

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-p', '--port', default=9000)

    options, list_args = parser.parse_args()

    config_mgr.init_values(filename='config.yaml')
    db_mgr.init_engine()

    server = tornado.httpserver.HTTPServer(application)
    server.bind(port=options.port)
    server.start(1)
    tornado.ioloop.IOLoop.current().start()
